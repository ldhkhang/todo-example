import { Component, OnInit } from '@angular/core';
import { ItemsService } from '../services/items.service';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { of } from 'rxjs';
@Component({
  selector: 'app-form-items',
  templateUrl: './form-items.component.html',
  styleUrls: ['./form-items.component.css']
})
export class FormItemsComponent implements OnInit {
  todoForm: FormGroup;
  constructor(private Items: ItemsService, private fb: FormBuilder) { }
  // @ContentChild(MatFormFieldControl) _control: MatFormFieldControl<any>;
  // @ViewChild(MatFormField) _matFormField: MatFormField;
addNewTodo() {
 // console.log(this.todoForm);
  if (this.todoForm.controls.userId.value && this.todoForm.controls.title.value && this.todoForm.controls.completed.value !== null) {
    if (this.todoForm.controls.completed.value === '') {
      this.todoForm.controls.completed.setValue(false);
    }
    const item = {
      userId: this.todoForm.controls.userId.value,
      id: 0,
      title: this.todoForm.controls.title.value,
      completed: this.todoForm.controls.completed.value
    };
    this.Items.addNewTodoItem(item);
    this.todoForm.controls.userId.setValue('');
    this.todoForm.controls.title.setValue('');
    this.todoForm.controls.completed.setValue(false);
  }
}
getErrorUserId() {
  return this.todoForm.controls.userId.hasError('max') ? 'Maximum is 10' :
      this.todoForm.controls.userId.hasError('pattern') ? 'Not accept zero number' :
      this.todoForm.controls.userId.hasError('required') ? 'Required' :
          '';
}
  ngOnInit() {
    this.todoForm = this.fb.group({
      userId : new FormControl( '', Validators.compose([Validators.required, Validators.max(10), Validators.pattern('^[1-9]*$')])),
      title : new FormControl( '', Validators.compose([Validators.required, Validators.minLength(10),Validators.maxLength(30)])),
      completed : ['']
      });
  }
}
