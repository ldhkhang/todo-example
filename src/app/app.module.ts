import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TodoItemsComponent } from './todo-items/todo-items.component';
import { ItemsService } from './services/items.service';
import { FormItemsComponent } from './form-items/form-items.component';
import { PipeIscompletePipe } from './pipes/pipe-iscomplete.pipe';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatCheckboxModule, MatFormFieldModule, MatInputModule, MatIconModule} from '@angular/material';
import { HttpClientModule  } from '@angular/common/http';
import { ReverseArrayPipe } from './pipes/reverseArray/reverse-array.pipe';
import { GenerateNameByUserIdPipe } from './pipes/generateNameByUserId/generate-name-by-user-id.pipe';
@NgModule({
  declarations: [
    AppComponent,
    TodoItemsComponent,
    FormItemsComponent,
    PipeIscompletePipe,
    ReverseArrayPipe,
    GenerateNameByUserIdPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    HttpClientModule

  ],
  providers: [
    ItemsService,
    HttpClientModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
