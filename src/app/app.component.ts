import { Component, OnInit, Output } from '@angular/core';
import { ItemsService } from './services/items.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'todo-example';
  itemsData: any;
  constructor(private Items: ItemsService) {}
 ngOnInit() {
  }
}
