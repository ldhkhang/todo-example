import { Component, OnInit, Input, ViewChild, OnChanges  } from '@angular/core';
import { ItemsTodo } from '../models/itemsTodoModel';
import { ELEMENT_MARKER } from '@angular/core/src/render3/interfaces/i18n';
import { ItemsService } from './../services/items.service';
@Component({
  selector: 'app-todo-items',
  templateUrl: './todo-items.component.html',
  styleUrls: ['./todo-items.component.css']
})

export class TodoItemsComponent implements OnInit {
 items = [];
constructor(private Items: ItemsService) {

}
deleteToDo(id: number) {
  console.log(id);
  this.Items.deleteTodoItem(id);
  this.items = this.Items.getAllItems();
}
  ngOnInit() {
 this.items = this.Items.getAllItems();
  }
}
