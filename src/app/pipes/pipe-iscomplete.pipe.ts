import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pipeIscomplete'
})
export class PipeIscompletePipe implements PipeTransform {

  transform(isComplete: boolean): any {
    if (isComplete === false) {
      return 'Chua Hoan Thanh';
    } else if (isComplete === true) {
      return 'Hoan Thanh';
    } else {
      return 'Khong Xac Dinh';
    }
  }

}
