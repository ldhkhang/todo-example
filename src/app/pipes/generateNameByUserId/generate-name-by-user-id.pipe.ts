import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'generateNameByUserId'
})
export class GenerateNameByUserIdPipe implements PipeTransform {

  transform(userId: number): any {
    if (userId !== null) {
      switch (userId) {
        case 1:
        return 'Alex';
        case 2:
        return 'Momo';
        case 3:
        return 'Loco';
        case 4:
        return 'Tim';
        case 5:
        return 'Ive';
        case 6:
        return 'Liliana';
        case 7:
        return 'Pink';
        case 8:
        return 'Adam Young';
        case 9:
        return 'Carly';
        case 10:
        return 'Calvin';
        default:
        return 'Unknown';
      }
     }
  }

}
