import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'reverseArray'
})
export class ReverseArrayPipe implements PipeTransform {

  transform(arrayNeedReverse): any {
    if (arrayNeedReverse) {
      return arrayNeedReverse.reverse();
    } else {
      return null;
    }
  }

}
