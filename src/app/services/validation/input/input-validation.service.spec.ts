import { TestBed } from '@angular/core/testing';

import { InputValidationService } from './input-validation.service';

describe('InputValidationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InputValidationService = TestBed.get(InputValidationService);
    expect(service).toBeTruthy();
  });
});
