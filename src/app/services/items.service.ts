import { Injectable } from '@angular/core';
import { Content } from '@angular/compiler/src/render3/r3_ast';
import { ItemsTodo } from '../models/itemsTodoModel';
import { Observable } from 'rxjs/Observable';
import {  HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class ItemsService {
  todoItems: ItemsTodo[];

  public getJSON(): Observable<any> {
    return this.http.get('http://jsonplaceholder.typicode.com/todos');
  }
  getAllItems() {
    return this.todoItems;
  }
  addNewTodoItem(item: ItemsTodo) {
    this.todoItems.push(item);
  }
  deleteTodoItem(id: number) {
   this.todoItems = this.todoItems.filter(value => value.id !== id );
  }
  constructor(private http: HttpClient) {
    this.todoItems = [{userId: 1, id: 1, title: "delectus aut autem", completed: false},
   {userId: 1, id: 2, title: "quis ut nam facilis et officia qui", completed: false},
   {userId: 1, id: 3, title: "fugiat veniam minus", completed: false},
   {userId: 1, id: 4, title: "et porro tempora", completed: true},
   {userId: 1, id: 5, title: "laboriosam mollitia et enim quasi adipisci quia provident illum", completed: false},
   {userId: 1, id: 6, title: "qui ullam ratione quibusdam voluptatem quia omnis", completed: false},
   {userId: 1, id: 7, title: "illo expedita consequatur quia in", completed: false},
   ];
    this.getJSON().subscribe(jsonData => {
      for (const i in jsonData) {
        if (jsonData[i]) {
          this.todoItems.push(jsonData[i]);
          }
      }
    }, error => console.log(error));
  }

}
